FROM debian:jessie

MAINTAINER Florian Margaine <florian@margaine.com>

RUN apt-get update --fix-missing

RUN apt-get -y install sbcl wget libfuse-dev devscripts build-essential

RUN rm -rf /var/lib/apt/lists/*
