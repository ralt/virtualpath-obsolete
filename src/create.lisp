(in-package #:virtualpath)

(defcommand create (path docker-image)
  "create a virtualpath folder"
  ;; The sanest way to make sure the docker image exists
  ;; is to run a dummy command in it.
  (handler-case
      ;; This is an ephemeral container, nothing to cleanup.
      (docker-run docker-image :command "ls")
    (error ()
      (quit 1 "The docker image ~s doesn't exist" docker-image)))
  (handler-case
      (let ((normalized-path (cat path "/")))
        (ensure-directories-exist normalized-path)
        (let ((activate-script (merge-pathnames "activate.bash" normalized-path)))
          (unless (probe-file activate-script)
            (write-string-into-file (djula:render-template* *activate-script* nil
                                                            :image docker-image)
                                    activate-script))))
    (error (e)
      (quit 2 "Error creating the virtualpath folder: ~a" e))))
