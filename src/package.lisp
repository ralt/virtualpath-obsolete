(defpackage #:virtualpath
  (:use #:cl #:alexandria))

(in-package #:virtualpath)

(defun cat (&rest args)
  (apply #'concatenate 'string args))
