(in-package #:virtualpath)

(djula:add-template-directory
 (asdf:system-relative-pathname (string-downcase (package-name *package*))
                                "templates/"))

(defvar *activate-script* (djula:compile-template* "activate.bash"))

(defvar *run-script* (djula:compile-template* "run"))
