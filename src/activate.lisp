(in-package #:virtualpath)

(defvar *binaries* (make-hash-table :test #'equal)
  "List of binaries available in the FUSE path.")

(defvar *image* nil
  "The docker image used by the FUSE filesystem.")

(defcommand activate (docker-image fuse-path)
  "activate a virtualpath in the current environment"
  (handler-case
      (dolist (file (ppcre:split #\Newline
                                 (docker-run docker-image
                                             :command "compgen -c")))
        (setf (gethash file *binaries*) (make-hash-table)))
    (error (e)
      (quit 3 "Unable to fetch the list of binaries in ~s" docker-image)))
  (setf *image* docker-image)
  (handler-case
      (cl-fuse:fuse-run `("virtualpathfs" ,fuse-path)
                        :directory-content 'directory-content
                        :directoryp 'directoryp
                        :symlink-target nil
                        :symlinkp nil
                        :file-open 'file-open
                        :file-release 'file-release
                        :file-read 'file-read
                        :file-size 'file-size
                        :file-write nil
                        :file-write-whole nil
                        :file-writeable-p nil
                        :file-executable-p 'file-executable-p
                        :file-create nil
                        :chmod nil
                        :chown nil
                        :truncate nil
                        :file-flush 'file-flush
                        :mkdir nil
                        :unlink nil
                        :rmdir nil
                        :symlink nil
                        :rename nil)
    (error (e)
      (quit 4 "Error running the FUSE filesystem: ~a" e))))

(defun directory-content (path)
  (declare (ignore path))
  (hash-table-keys *binaries*))

(defun directoryp (path)
  (not path))

(defun file-open (path flags)
  (declare (ignore flags))
  (unless (= (length path) 1)
    (return-from file-open (- cl-fuse:error-ENOENT)))
  (multiple-value-bind (_ present)
      (gethash (first path) *binaries*)
    (declare (ignore _))
    (if present 0 (- cl-fuse:error-ENOENT))))

(defun file-release (path flags)
  ;; @TODO find out what this is for.
  0)

(defun file-read (path size offset fh)
  (declare (ignore fh))
  (unless (= (length path) 1)
    (return-from file-read (- cl-fuse:error-ENOENT)))
  (let ((file-table (file-read-inner path)))
    (when (typep file-table 'integer)
      (return-from file-read file-table))
    (let ((file-contents (gethash 'content file-table))
          (file-length (gethash 'size file-table)))
      (subseq file-contents
              offset
              (if (> file-length (+ offset size))
                  (+ offset size)
                  file-length)))))

(defun file-read-inner (path)
  (multiple-value-bind (file-table present)
      (gethash (first path) *binaries*)
    (unless present
      (return-from file-read-inner (- cl-fuse:error-ENOENT)))
    (multiple-value-bind (_ present)
        (gethash 'content file-table)
      (declare (ignore _))
      (unless present
        (setf (gethash 'content file-table)
              (djula:render-template* *run-script* nil
                                      :image *image*))
        (setf (gethash 'size file-table)
              (length (gethash 'content file-table))))
      file-table)))

(defun file-size (path)
  (unless (= (length path) 1)
    (return-from file-size (- cl-fuse:error-ENOENT)))
  (multiple-value-bind (file-table present)
      (gethash (first path) *binaries*)
    (unless present
      (return-from file-size (- cl-fuse:error-ENOENT)))
    (multiple-value-bind (size present)
        (gethash 'size file-table)
      (unless present
        (setf size (gethash 'size (file-read-inner path))))
      size)))

(defun file-executable-p (path)
  t)

(defun file-flush (path fh)
  ;; @TODO figure out what this is for.
  0)
