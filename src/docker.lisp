(in-package #:virtualpath)

(defun shell-command (shell)
  (switch (shell :test #'string=)
    ("bash" "/bin/bash -c")))

(defun run-with-output (&rest args)
  (with-output-to-string (s)
    (apply #'uiop:run-program (append args `(:output ,s)))
    s))

(defun docker-run (image &key (shell "bash") command)
  (run-with-output (format
                    nil
                    "docker run --rm=true -i ~a ~a ~s"
                    image
                    (shell-command shell)
                    command)))
