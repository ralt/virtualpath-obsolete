(in-package #:virtualpath)

(defvar *commands* (make-hash-table :test #'equal)
  "List of commands supported by virtualpath.")

(defmacro defcommand (name vars description &body body)
  `(setf (gethash ,(string-downcase (symbol-name name)) *commands*)
         (list :function (lambda ,vars
                           ,@body)
               :description ,description
               :args-count ,(length vars))))
