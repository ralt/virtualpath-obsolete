(in-package #:virtualpath)

(export 'main)
(defun main ()
  (let ((parsed-args (apply-argv:parse-argv* uiop:*command-line-arguments*)))
    (when (or (eq parsed-args nil)
              (not (stringp (first parsed-args)))
              (multiple-value-bind (_ present)
                  (gethash (first parsed-args) *commands*)
                (declare (ignore _))
                (not present)))
      (return-from main (help)))
    (handler-case
        (apply (getf (gethash (first parsed-args) *commands*) :function)
               (rest parsed-args))
      (error ()
        ;; This assumes that every command properly manages errors.
        ;; In theory, if we end up in this path, it just means that
        ;; the arguments passed to the command don't match the number
        ;; it expects.
        (help)))))

(defun help ()
  (format t "Usage: virtualpath create <folder> <docker image>

Once you have created the folder, you can activate your virtualpath with:

~t~tsource <folder>/activate.bash

To exit the virtualpath, you can run:

~t~tdeactivate

If you find any bug, please send an email to <virtualpath-project@googlegroups.com>
"))
