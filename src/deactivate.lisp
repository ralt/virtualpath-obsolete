(in-package #:virtualpath)

(defcommand deactivate (path)
  "deactivate a virtualpath"
  (handler-case
      (uiop:run-program (format nil "fusermount -u ~a" path))
    (error ()
      (quit 4 "Failed to umount the fuse directory. Are you sure fusermount is installed?"))))
