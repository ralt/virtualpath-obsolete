(in-package #:virtualpath)

(defun quit (exit-code message &rest args)
  (apply #'format t (cat message "~%") args)
  (uiop:quit exit-code))
