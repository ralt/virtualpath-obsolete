(asdf:defsystem #:virtualpath
  :description "Woo"
  :author "Florian Margaine <florian@margaine.com>"
  :license "GNU General Public License v3.0"
  :serial t
  :build-operation asdf:program-op
  :build-pathname "bin/virtualpath"
  :entry-point "virtualpath:main"
  :depends-on (:cl-fuse-meta-fs :alexandria :djula :cl-ppcre :apply-argv :uiop)
  :components ((:module "src"
                :components ((:file "package")
                             (:file "main" :depends-on ("package"
                                                        "activate"
                                                        "create"))
                             (:file "activate" :depends-on ("package"
                                                            "command"
                                                            "error"
                                                            "docker"))
                             (:file "create" :depends-on ("package"
                                                          "command"
                                                          "error"
                                                          "docker"
                                                          "templates"))
                             (:file "deactivate" :depends-on ("package"
                                                              "command"
                                                              "error"))
                             (:file "command" :depends-on ("package"))
                             (:file "error" :depends-on ("package"))
                             (:file "templates" :depends-on ("package"))
                             (:file "docker" :depends-on ("package"))))))
