DESTDIR := /usr/local
SOURCES := $(wildcard src/*.lisp) $(wildcard *.asd) $(wildcard templates/*)
QL_LOCAL=$(PWD)/.quicklocal/quicklisp
SBCL_OPTS=--noinform --noprint --no-sysinit --no-userinit --disable-debugger
QL_OPTS=--load $(QL_LOCAL)/setup.lisp
NAME=virtualpath
BINARY_FOLDER=bin
PWD=$(shell pwd)

all: $(BINARY_FOLDER)/$(NAME)

deps:
	sbcl $(SBCL_OPTS) $(QL_OPTS) \
		--eval '(push "$(PWD)/" asdf:*central-registry*)' \
		--eval '(ql:quickload :$(NAME))' \
		--eval '(quit)'
	touch $@

$(BINARY_FOLDER):
	mkdir -p $(BINARY_FOLDER)

$(BINARY_FOLDER)/$(NAME): $(SOURCES) $(QL_LOCAL)/setup.lisp deps $(BINARY_FOLDER)
	sbcl $(SBCL_OPTS) $(QL_OPTS) \
		--eval '(push "$(PWD)/" asdf:*central-registry*)' \
		--eval "(asdf:operate 'asdf:build-op :$(NAME))" \
		--eval '(quit)'

.PHONY: clean install dist

install: $(BINARY_FOLDER)/$(NAME)
	mkdir -p $(DESTDIR)/bin
	cp $(BINARY_FOLDER)/$(NAME) $(DESTDIR)/bin/$(NAME)

clean:
	rm -rf deps .quicklocal $(BINARY_FOLDER) quicklisp.lisp

$(QL_LOCAL)/setup.lisp: quicklisp.lisp
	sbcl $(SBCL_OPTS) \
		--load quicklisp.lisp \
		--eval '(quicklisp-quickstart:install :path "$(QL_LOCAL)")' \
		--eval '(quit)'

quicklisp.lisp:
	wget https://beta.quicklisp.org/quicklisp.lisp
	echo '4a7a5c2aebe0716417047854267397e24a44d0cce096127411e9ce9ccfeb2c17 *quicklisp.lisp' | shasum -c -

virtualpath-debian-builder: Dockerfile
	docker build -t virtualpath-debian-builder .
	touch $@

.virtualpath/activate.bash: $(BINARY_FOLDER)/$(NAME) virtualpath-debian-builder
	$(BINARY_FOLDER)/$(NAME) create .virtualpath/ virtualpath-debian-builder:latest

dist: .virtualpath/activate.bash dist/$(NAME).1.gz
	rm -rf .quicklocal/
	source .virtualpath/activate.bash && sleep 5 && run_in_virtualpath dpkg-buildpackage -b -us -uc; deactivate
	rm -rf .quicklocal/

dist/$(NAME).1: docs/$(NAME).md
	mkdir -p dist/
	pandoc -s -t man $< > $@

dist/$(NAME).1.gz: dist/$(NAME).1
	gzip --keep $<
